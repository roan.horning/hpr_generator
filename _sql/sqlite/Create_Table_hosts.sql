-- hosts definition

CREATE TABLE hosts (
	hostid INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	host TEXT NOT NULL,
	email TEXT NOT NULL,
	profile TEXT NOT NULL,
	license VARCHAR(11) DEFAULT 'CC-BY-SA',
	local_image INTEGER DEFAULT 0 NOT NULL,
	gpg TEXT NOT NULL,
	espeak_name TEXT
);
